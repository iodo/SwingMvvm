package dev.iodo.lab.swingmvvm.viewmodels;

import static dev.iodo.lab.swingmvvm.views.IView.PropertyId;
import static dev.iodo.lab.swingmvvm.views.IView.PropertyName;
import static dev.iodo.lab.swingmvvm.views.IView.PropertyOutput;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dev.iodo.lab.swingmvvm.models.ModelMain;

public class ViewModelMain implements IViewModel {

	///////////////////////////////////////////////////////////////////////////
	// Attributes
	//
	private ModelMain model;
	private String output;
	private PropertyChangeSupport listener;
	private static Logger logger = LoggerFactory.getLogger(ViewModelMain.class);

	///////////////////////////////////////////////////////////////////////////
	// Ctors
	//
	public ViewModelMain() {
		initialize();
	}

	///////////////////////////////////////////////////////////////////////////
	// PropertyChangeListener
	//
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		this.listener.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
		this.listener.addPropertyChangeListener(propertyName, listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		this.listener.removePropertyChangeListener(listener);
	}

	///////////////////////////////////////////////////////////////////////////
	// Implementations
	//

	///////////////////////////////////////////////////////////////////////////
	// Procedures
	//
	
	public void login() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Id : ");
		buffer.append(model.getId());
		buffer.append("\nName : ");
		buffer.append(model.getName());
		
//		JOptionPane.showMessageDialog(
//				null, 
//				buffer.toString(), 
//				"Informatrion", 
//				JOptionPane.INFORMATION_MESSAGE);
		
		setOutput(buffer.toString());
	}
	
	private void initialize() {
		listener = new PropertyChangeSupport(this);
		model = new ModelMain();
	}

	///////////////////////////////////////////////////////////////////////////
	// Accessors
	//
	public String getId() {
		return model.getId();
	}
	
	public String getName() {
		return model.getName();
	}
	
	public String getOutput() {
		return output;
	}

	///////////////////////////////////////////////////////////////////////////
	// Mutators
	//
	public void setId(String id) {
		String old = model.getId(); 
		model.setId(id);
		listener.firePropertyChange(PropertyId, old, id);
	}
	
	public void setName(String name) {
		String old = model.getName(); 
		model.setName(name);
		listener.firePropertyChange(PropertyName, old, name);
		
	}
	
	public void setOutput(String output) {
		String old = this.output;
		this.output = output;
		listener.firePropertyChange(PropertyOutput, old, output);
	}
}
