package dev.iodo.lab.swingmvvm.views;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//https://gist.github.com/jeromerodrigo/2516870
import dev.iodo.lab.swingmvvm.viewmodels.ViewModelMain;

@SuppressWarnings("serial")
public class ViewMain extends JFrame implements IView, PropertyChangeListener {
	
	private final int width  = 330;
	private final int height = 200;
	
	private ViewModelMain viewModel;
	private JTextField textfieldName;
	private JTextField textfieldId;
	private JTextArea textAreaOutput;
	private Font fontSystem;
	
	private static Logger logger = LoggerFactory.getLogger(ViewMain.class);
	///////////////////////////////////////////////////////////////////////////
	// Ctors
	//
	public ViewMain(String title) throws HeadlessException {
		super(title);
		initialize();
	}
	
	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		logger.debug("Received property changed Event");
		logger.debug("Property  = [" + evt.getPropertyName() + "]");
		logger.debug("Old value = [" + evt.getOldValue() + "]");
		logger.debug("New Value = [" + evt.getNewValue() + "]");
		
		
		final String target = evt.getPropertyName();
		switch(target)
		{
		case PropertyName:
			textfieldName.setText(evt.getNewValue().toString());
			break;
			
		case PropertyId:
			textfieldId.setText(evt.getNewValue().toString());
			break;
			
		case PropertyOutput:
			textAreaOutput.setText(evt.getNewValue().toString());
			break;
			
		default:
			break;
		}
		
	}
	
	///////////////////////////////////////////////////////////////////////////
	// Procedures
	//
	public void initialize() {		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(width, height);
		setResizable(false);
		
		viewModel = new ViewModelMain();
		viewModel.addPropertyChangeListener(this);
		
		fontSystem = new Font("Arial", Font.PLAIN, 12);
		
		getRootPane().setBorder(BorderFactory.createEmptyBorder(10,  10,  10, 10));
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(buildLayoutInformation(), BorderLayout.NORTH);
		getContentPane().add(buildLayoutOutput(), BorderLayout.CENTER);
		
		///////////////////////////////////////////////////////////////////////
		// Initialize Data Binding
		bindingData();
	}
	
	
	private JComponent buildLayoutInformation() {
		JPanel panel = new JPanel();
		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		panel.setLayout(gbl);
		
		gbc.gridy = 0;
		gbc.gridx = 0;
		gbc.weightx = 1.;
		gbc.anchor = GridBagConstraints.WEST;
		JLabel labelID = new JLabel("Id");
		labelID.setFont(fontSystem);
		gbl.setConstraints(labelID, gbc);
		panel.add(labelID);
		
		gbc.insets = new Insets(0, 10, 0, 0);
		gbc.gridy = 0;
		gbc.gridx = 1;
		gbc.weightx = 100.;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		textfieldId = new JTextField();
		textfieldId.setFont(fontSystem);
		gbl.setConstraints(textfieldId, gbc);
		panel.add(textfieldId);
		
		gbc.insets = new Insets(10, 0, 10, 0);
		gbc.gridy = 1;
		gbc.gridx = 0;
		gbc.weightx = 1.;
		gbc.anchor = GridBagConstraints.WEST;
		JLabel labelName = new JLabel("Name");
		labelName.setFont(fontSystem);
		gbl.setConstraints(labelName, gbc);
		panel.add(labelName);
		
		gbc.insets = new Insets(0, 10, 0, 0);
		gbc.gridy = 1;
		gbc.gridx = 1;
		gbc.weightx = 100.;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		textfieldName = new JTextField();
		textfieldName.setFont(fontSystem);
		gbl.setConstraints(textfieldName, gbc);
		panel.add(textfieldName);
		
		gbc.insets = new Insets(10, 0, 10, 0);
		gbc.gridy = 2;
		gbc.gridx = 0;
		gbc.gridwidth = 2;
		
		gbc.weightx = 1.;
		gbc.anchor = GridBagConstraints.CENTER;
		
		JButton buttonConfirm = new JButton("Confirm");
		gbl.setConstraints(buttonConfirm, gbc);

//		buttonConfirm.addActionListener(new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				viewModel.setId(textfieldId.getText());
//				viewModel.setName(textfieldName.getText());
//				viewModel.login();
//			}
//		});
		
		buttonConfirm.addActionListener(e->{
			viewModel.setId(textfieldId.getText());
			viewModel.setName(textfieldName.getText());
			viewModel.login();
		});
		
		panel.add(buttonConfirm);
		return panel;
	}
	
	private JComponent buildLayoutOutput() {
		textAreaOutput = new JTextArea();
		textAreaOutput.setFont(fontSystem);
		return new JScrollPane(textAreaOutput); 
	}
	
	private void bindingData() {
		viewModel.setId("Test Id");
		viewModel.setName("Test Name");
	}
}
