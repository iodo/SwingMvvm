package dev.iodo.lab.swingmvvm.views;

public interface IView {
	final static String PropertyId = "ID";
	final static String PropertyName = "NAME";
	final static String PropertyOutput = "Output";
}
